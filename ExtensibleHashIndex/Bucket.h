#ifndef BUCKET_H
#define BUCKET_H

#include <iostream>
#include <cstdlib>

class Bucket{
    public:
        Bucket(int maxLength); // default constructor
        ~Bucket(); // destructor
        Bucket(const Bucket& copyBucket);
        Bucket& operator= (const Bucket& copyBucket);
        //void insertKey(int keyToInsert);
        //might need some accessor and mutator functions
        bool find(int searchKey); // perform linear search on array
        void remove(int removeKey); // remove key by searching array
        void print(); // print contents of bucket
        void insert(int insertKey);
        int getLength() const;
        int getMaxLength() const;
        int getLocalDepth() const;
        int getBucketIndex(int index) const;
        void setBucketIndex(int index, int setKey);
        bool allSame(int searchKey);
        void setLocalDepth(int newDepth);
        void setLength(int newLength);
        void reorganize(); // reorganize the bucket removing empty spots

    private:
        int length;
        int maxLength;
        int localDepth;
        int* bucket; // array containing search keys
};

#endif