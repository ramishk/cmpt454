#include "ExtensibleHashTable.h"

using namespace std;

#define empty -1

ExtensibleHashTable::ExtensibleHashTable() : globalDepth(1), maxBucketLength(4){
    directory = new Bucket*[2];
    for(int i = 0; i < 2; i++){
        directory[i] = new Bucket(4);
    }
}

ExtensibleHashTable::ExtensibleHashTable(int length) : globalDepth(1), maxBucketLength(length){
    directory = new Bucket*[2];
    for(int i = 0; i < 2; i++){
        directory[i] = new Bucket(length);
    }
}

ExtensibleHashTable::ExtensibleHashTable(int length, int newGlobalDepth) : globalDepth(newGlobalDepth), maxBucketLength(length){
    directory = new Bucket*[1 << newGlobalDepth];
    for(int i = 0; i < (1 << newGlobalDepth); i++){
        directory[i] = new Bucket(length);
    }
}

ExtensibleHashTable::~ExtensibleHashTable(){
    delete[] directory;
}

ExtensibleHashTable::ExtensibleHashTable(const ExtensibleHashTable& copyHashTable){
    delete[] directory;
    directory = new Bucket*[1 << copyHashTable.getGlobalDepth()];
    globalDepth = copyHashTable.getGlobalDepth();
    maxBucketLength = copyHashTable.getMaxBucketLength();
    for(int i = 0; i < (1 << globalDepth); i++){
        directory[i] = new Bucket(maxBucketLength);
        directory[i] = copyHashTable.getDirectoryEntry(i);
    }
}

ExtensibleHashTable& ExtensibleHashTable::operator=(const ExtensibleHashTable& hashTable){
    ExtensibleHashTable* newHashTable = new ExtensibleHashTable(hashTable.getMaxBucketLength(), hashTable.getGlobalDepth());
    newHashTable->globalDepth = hashTable.getGlobalDepth();
    newHashTable->maxBucketLength = hashTable.getMaxBucketLength();
    for(int i = 0; i < (1 << globalDepth); i++){
        directory[i] = new Bucket(maxBucketLength);
        directory[i] = hashTable.getDirectoryEntry(i);
    }   
    return *newHashTable;
}

bool ExtensibleHashTable::find(int searchKey){
    int directoryIndex = FindIndex(searchKey, globalDepth);
    return directory[directoryIndex]->find(searchKey);
}

void ExtensibleHashTable::insert(int insertKey){
    int directoryIndex = FindIndex(insertKey, globalDepth);
    if(directory[directoryIndex]->getLength() < directory[directoryIndex]->getMaxLength()){ // current length is less than the maxLength, insert
        directory[directoryIndex]->insert(insertKey);
        return;
    }
    // check for throwing exception
    if(directory[directoryIndex]->getLocalDepth() < globalDepth){ // bucket full but local depth < global depth
        // increase local depth and rearrange bucket
        try{
            if(directory[directoryIndex]->allSame(insertKey) == true){
                throw runtime_error("Cannot insert, bucket full with the same key. ABORTED");
                return;
            }
        }
        catch(runtime_error& error){
            cout << "Exception thrown: " << error.what() << endl;
        }
        splitBucket(directoryIndex, insertKey);
        directoryIndex = FindIndex(insertKey, globalDepth);
        directory[directoryIndex]->insert(insertKey);
    }
    else{ // bucket full and global depth == local depth, double directory
        try{
            if(directory[directoryIndex]->allSame(insertKey) == true){
                throw runtime_error("Cannot insert, bucket full with the same key. ABORTED");
            }
        }
        catch(runtime_error& error){
            cout << "Exception thrown: " << error.what() << endl;
            return;
        }
        DoubleDirectory();
        splitBucket(directoryIndex);
        directoryIndex = FindIndex(insertKey, globalDepth);
        directory[directoryIndex]->insert(insertKey);
    }
}

bool ExtensibleHashTable::remove(int removeKey){
    int directoryIndex = FindIndex(removeKey, globalDepth);
    if(find(removeKey) == false){ // key is not in table
        return false;
    }
    // key is in table remove
    directory[directoryIndex]->remove(removeKey);
    return true;
}

void ExtensibleHashTable::print(){
    map<Bucket*, int> printedBuckets;
    for(int i = 0; i < (1 << globalDepth); i++){
        void* temp = (void*)directory[i];
        cout << i << ": " << noshowbase << hex  << uppercase << reinterpret_cast<intptr_t>(temp) << dec << " --> ";
        if(printedBuckets.find(directory[i]) != printedBuckets.end()){ // this bucket has already been printed
            cout << endl;
            continue;
        }
        printedBuckets.insert({directory[i], globalDepth});
        directory[i]->print();
        cout << endl;
    }
    cout << endl;
}

int ExtensibleHashTable::FindIndex(int key, int globalDepth){
    string directoryIndexBinary;
    // make decimal to binary string
    directoryIndexBinary = to_string(key % 2);
    key = key / 2;
    for(int i = 1; i < globalDepth; i++){
        directoryIndexBinary += to_string(key % 2);
        key = key / 2;
    }
    int directoryIndex = 0;
    for(int i = 0; i < globalDepth; i++){ // make binary back to decimal up till the globalDepth
        int directoryIndexInt = directoryIndexBinary[i] - '0';
        directoryIndex += directoryIndexInt * pow(2,i);
    }
    return directoryIndex;
}

int ExtensibleHashTable::getGlobalDepth() const{
    return globalDepth;
}

int ExtensibleHashTable::getMaxBucketLength() const{
    return maxBucketLength;
}

Bucket* ExtensibleHashTable::getDirectoryEntry(int index) const{
    return directory[index];
}

void ExtensibleHashTable::splitBucket(int directoryIndex){
    directory[directoryIndex]->setLocalDepth(directory[directoryIndex]->getLocalDepth() + 1); // increment local depth
    int newDirectoryIndexBucket = 1 << (directory[directoryIndex]->getLocalDepth() - 1);
    newDirectoryIndexBucket += directoryIndex;
    directory[newDirectoryIndexBucket] = new Bucket(maxBucketLength);
    directory[newDirectoryIndexBucket]->setLength(0);
    directory[newDirectoryIndexBucket]->setLocalDepth(directory[directoryIndex]->getLocalDepth());

    for(int i = 0; i < maxBucketLength; i++){
        int key = directory[directoryIndex]->getBucketIndex(i);
        int newDirectoryIndex = FindIndex(key, directory[directoryIndex]->getLocalDepth());
        if(newDirectoryIndex != directoryIndex){ // going in the new directory
                directory[directoryIndex]->setBucketIndex(i, empty); // delete from old bucket
                directory[newDirectoryIndex]->insert(key); // insert into new bucket
        }
    }
    directory[directoryIndex]->reorganize();
    return;
}

void ExtensibleHashTable::splitBucket(int directoryIndex, int insertKey){
    directoryIndex = FindIndex(insertKey, directory[directoryIndex]->getLocalDepth());
    directory[directoryIndex]->setLocalDepth(directory[directoryIndex]->getLocalDepth() + 1); // increment local depth
    int newDirectoryIndexBucket = 1 << (directory[directoryIndex]->getLocalDepth() - 1);
    newDirectoryIndexBucket += directoryIndex;
    directory[newDirectoryIndexBucket] = new Bucket(maxBucketLength);
    directory[newDirectoryIndexBucket]->setLength(0);
    directory[newDirectoryIndexBucket]->setLocalDepth(directory[directoryIndex]->getLocalDepth());
    for(int i = 0; i < maxBucketLength; i++){
        int key = directory[directoryIndex]->getBucketIndex(i);
        int newDirectoryIndex = FindIndex(key, directory[directoryIndex]->getLocalDepth());
        if(newDirectoryIndex != directoryIndex){ // going in the new directory
                directory[directoryIndex]->setBucketIndex(i, empty); // delete from old bucket
                directory[newDirectoryIndex]->insert(key); // insert into new bucket
        }
    }
    directory[directoryIndex]->reorganize();
    return;
}

void ExtensibleHashTable::DoubleDirectory(){
    globalDepth++;
    Bucket** newDirectory = new Bucket*[1 << globalDepth];
    for(int i = 0; i < 1 << (globalDepth - 1); i++){
        newDirectory[i] = directory[i];
    }
    int j = 0;
    for(int i = 1 << (globalDepth - 1); i < 1 << globalDepth; i++){
        newDirectory[i] = directory[j];
        j++;
    }
    delete[] directory;
    directory = newDirectory;
    return;
}