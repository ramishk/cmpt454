#ifndef EXTHASHTABLE
#define EXTHASHTABLE

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <string>
#include <map>
#include "Bucket.h"

class ExtensibleHashTable{
    public:
        ExtensibleHashTable(); // default constructor
        ExtensibleHashTable(int length);
        ExtensibleHashTable(int length, int newGlobalDepth);
        ~ExtensibleHashTable(); // destructor
        ExtensibleHashTable(const ExtensibleHashTable& copyHashTable); // deep copy constructor
        ExtensibleHashTable& operator= (const ExtensibleHashTable& hashTable); // overloaded = operator
        bool find(int searchKey);
        void insert(int insertKey);
        bool remove(int removeKey);
        void print();
        int getGlobalDepth() const;
        int getMaxBucketLength() const;
        Bucket* getDirectoryEntry(int index) const;
        

    private:
        int globalDepth;
        int maxBucketLength; // maybe unecesscary here
        Bucket** directory; // array of Bucket classes
        
        int FindIndex(int key, int globalDepth);
        void splitBucket(int directoryIndex);
        void splitBucket(int directoryIndex, int insertKey);
        void DoubleDirectory();
};

#endif