#include "Bucket.h"

using namespace std;

#define empty -1

Bucket::Bucket(int maxLength) : length(0), maxLength(maxLength), localDepth(1){
    bucket = new int[maxLength];
    for(int i = 0; i < maxLength; i++){
        bucket[i] = empty;
    }
}

Bucket::~Bucket(){
    delete[] bucket;
}

Bucket::Bucket(const Bucket& copyBucket){
    delete[] bucket;
    bucket = new int[copyBucket.getMaxLength()];
    maxLength = copyBucket.getMaxLength();
    length = copyBucket.getLength();
    localDepth = copyBucket.getLocalDepth();
    for(int i = 0; i < maxLength; ){
        bucket[i] = copyBucket.getBucketIndex(i);
    }
}
           
Bucket& Bucket::operator=(const Bucket& copyBucket){
    Bucket* newBucket = new Bucket(copyBucket.getLength());
    newBucket->length = copyBucket.getLength();
    newBucket->localDepth = copyBucket.getLocalDepth();
    newBucket->maxLength = copyBucket.getMaxLength();
    delete[] newBucket->bucket;
    newBucket->bucket = new int[maxLength];
    for(int i = 0; i < maxLength; ){
        bucket[i] = copyBucket.getBucketIndex(i);
    }
    return *newBucket;
}

bool Bucket::find(int searchKey){
    for(int i = 0; i < maxLength; i++){ // linear search through array for searchKey
        if(bucket[i] == searchKey){
            return true;
        }
    }
    return false;
}

void Bucket::remove(int removeKey){
    for(int i = 0; i < maxLength; i++){
        if(bucket[i] == removeKey){ // anywhere the removeKey is in the array is now -1
            bucket[i] = empty;
            length--;
        }
    }
    if(length != 0){
        reorganize(); // reorganize the new empty spots
    }
}

void Bucket::reorganize(){
    int* newBucket = new int[maxLength];
    int j = 0;
    for(int i = 0; i < maxLength; i++){
        if(bucket[i] != empty){
            newBucket[j] = bucket[i];
            j++;
        }
    }
    for(int i = j; i < maxLength; i++){
        newBucket[i] = empty;
    }
    delete[] bucket;
    bucket = newBucket;
    return;
}

void Bucket::print(){
    cout << "[";
    for(int i = 0; i < maxLength; i++){
        if(bucket[i] == empty){
            cout << "-";
        }
        else{
            cout << bucket[i];
        }
        if(i != maxLength - 1){
            cout << ",";
        }
    }
    cout << "] (" << localDepth << ")";
    return;
}

void Bucket::insert(int insertKey){
    for(int i = 0; i < maxLength; i++){
        if(bucket[i] == empty){
            bucket[i] = insertKey;
            length++;
            return;
        }
    }
}

int Bucket::getLength() const{
    return length;
}

int Bucket::getMaxLength() const{
    return maxLength;
}

int Bucket::getLocalDepth() const{
    return localDepth;
}

int Bucket::getBucketIndex(int index) const{
    return bucket[index];
}

bool Bucket::allSame(int searchKey){
    for (int i = 0; i < maxLength; i++){
        if(bucket[i] != searchKey){
            return false;
        }
    }
    return true;
}

void Bucket::setLocalDepth(int newDepth){
    localDepth = newDepth;
}

void Bucket::setBucketIndex(int index, int setKey){
    bucket[index] = setKey;
    if(setKey == empty){
        length--;
    }
    else{
        length++;
    }
}

void Bucket::setLength(int newLength){
    length = newLength;
}