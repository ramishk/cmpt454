#include <iostream>
#include <cstdlib>
#include "Bucket.h"
#include "ExtensibleHashTable.h"

using namespace std;

int main(int argc, char* argv[]){
    ExtensibleHashTable* exthash = new ExtensibleHashTable;
    exthash->print();
    exthash->insert(1);
    exthash->print();
    exthash->insert(0);
    exthash->insert(1);
    exthash->insert(2);
    exthash->print();
    if(exthash->find(2) == true){
        cout << "Found 2, correct" << endl;
    }
    else{
        cout << "Not found 2, incorrect" << endl;
    }
    if(exthash->find(4) == false){
        cout << "4 not found, correct" << endl;
    }
    else{
        cout << "4 found, incorrect" << endl;
    }
    
    if(exthash->remove(4) == false){
        cout << "4 not removed, correct" << endl;
    }
    else{
        cout << "4 removed, incorrect" << endl;
    }
    exthash->remove(2);
    exthash->remove(1);
    exthash->print();

    exthash->insert(5);
    exthash->print();
    
    exthash->insert(6);
    exthash->print();
    
    exthash->insert(8);
    exthash->print();
    
    exthash->insert(11);
    exthash->print();
    
    exthash->insert(14);
    exthash->print();
    
    exthash->insert(15);
    exthash->print();
    
    exthash->insert(17);
    exthash->print();
    
    exthash->insert(29);
    exthash->print();
    
    exthash->insert(35);
    exthash->print();
    
    

    ExtensibleHashTable table;
    table.insert(64);
    table.insert(14);
    table.insert(153);
    table.insert(200);
    table.insert(218);
    table.insert(67);
    table.insert(66);
    table.insert(253);
    table.insert(13);
    table.insert(128);
    table.insert(109);
    table.insert(10);
    table.insert(10);
    table.insert(4);
    table.insert(12);
    table.insert(99);
    table.insert(123);
    table.insert(108);
    table.print();

    return 0;
}